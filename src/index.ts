//max
export const max = (coll: number[]): number => {
  return coll.reduce((acc: number, val: number) => (acc > val ? acc : val));
};

//to array
export const toArray = <T>(...args: T[]): T[] =>
  args.reduce((prev: T[], curr: T) => [...prev, curr], []);

//unique elements
export const getUnique = <T>(acc: T[], val: T): T[] => {
  if (acc.indexOf(val) === -1) {
    return [...acc, val];
  } else {
    return [...acc];
  }
};

export const myUnique = <T>(arr: T[]): T[] => arr.reduce(getUnique, []);

//Map
export const myMap = <T, V>(fun: (a: T) => V, coll: T[]): V[] => {
  const res: V[] = [];
  for (const item of coll) {
    res.push(fun(item));
  }
  return res;
};

//filter
export const myFilter = <T, V>(fun: (a: T) => V, coll: T[]): T[] => {
  const result: T[] = [];
  for (const item of coll) {
    if (fun(item)) {
      result.push(item);
    }
  }
  return result;
};

//Reduce
export const myReduce = <T>(fun: (a: T, b: T) => T, init: T, coll: T[]): T => {
  let result: T = init;
  for (const item of coll) {
    result = fun(result, item);
  }
  return result;
};
