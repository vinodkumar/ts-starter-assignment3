import {
  myMap,
  myFilter,
  myReduce,
  max,
  toArray,
  getUnique,
  myUnique
} from './index';
describe('Max Of Numbers', () => {
  test('max of 1, 6 and 8', () => {
    expect(max([1, 6, 8])).toBe(8);
  });
  test('max of -1, 2 and -3', () => {
    expect(max([-1, 2, -3])).toBe(2);
  });
  test('max of -1, 0 and 1', () => {
    expect(max([-1, 0, -3])).toBe(0);
  });
  test('max of -10, -11, - 3 ', () => {
    expect(max([-10, -11, -3])).toBe(-3);
  });
  test('max of 0, 0 and 0', () => {
    expect(max([0, 0, 0])).toBe(0);
  });
});

describe('to Array', () => {
  test('toArray of 1, 2, 3', () => {
    expect(toArray<number>(1, 2, 3)).toEqual([1, 2, 3]);
  });
  test('toArray of a, b and c', () => {
    expect(toArray<string>('a', 'b', 'c')).toEqual(['a', 'b', 'c']);
  });
  test('toArray of ', () => {
    expect(toArray<string>()).toEqual([]);
  });
});

describe('Unique Elements', () => {
  test('getunique([],3)', () => {
    expect(getUnique<number>([], 3)).toEqual([3]);
  });

  test('getunique([7,6],6)', () => {
    expect(getUnique<number>([7, 6], 6)).toEqual([7, 6]);
  });
  test('getunique([1,2,3,4],8)', () => {
    expect(getUnique<number>([1, 2, 3, 4], 8)).toEqual([1, 2, 3, 4, 8]);
  });
  test('getunique([],a)', () => {
    expect(getUnique<string>([], 'a')).toEqual(['a']);
  });
  test('getunique([a,b,c,d],a)', () => {
    expect(getUnique<string>(['a', 'b', 'c', 'd'], 'a')).toEqual([
      'a',
      'b',
      'c',
      'd'
    ]);
  });
  test('getunique([a,b,c,d],e)', () => {
    expect(getUnique<string>(['a', 'b', 'c', 'd'], 'e')).toEqual([
      'a',
      'b',
      'c',
      'd',
      'e'
    ]);
  });

  test('unique([1, 3, 3, 5, 2, 1, 4])', () => {
    expect(myUnique<number>([1, 3, 3, 5, 2, 1, 4])).toEqual([1, 3, 5, 2, 4]);
  });
  test('unique([])', () => {
    expect(myUnique<number>([])).toEqual([]);
  });
  test('unique([1, 3, 3, 5, 2, 1, 4])', () => {
    expect(myUnique<number>([1, 3, 3, 5, 2, 1, 4])).toEqual([1, 3, 5, 2, 4]);
  });
  test('unique([a, b ,c ,d, a, d])', () => {
    expect(myUnique<string>(['a', 'b', 'c', 'b', 'e', 'a'])).toEqual([
      'a',
      'b',
      'c',
      'e'
    ]);
  });
  test('unique([])', () => {
    expect(myUnique<string>([])).toEqual([]);
  });
});

describe('map', () => {
  test('myMap<number, number>(x => x + 1, [1, 2, 3, 4, 5]) -> [2, 3, 4, 5, 6]', () => {
    expect(myMap<number, number>(x => x + 1, [1, 2, 3, 4, 5])).toEqual([
      2,
      3,
      4,
      5,
      6
    ]);
  });

  test('myMap<number, number>(x => x * 2, [1, 2, 3, 4, 5]) -> [2, 4, 6, 8, 10]', () => {
    expect(myMap<number, number>(x => x * 2, [1, 2, 3, 4, 5])).toEqual([
      2,
      4,
      6,
      8,
      10
    ]);
  });
  test('myMap<number, string>(x => x + #, [1, 2, 3, 4]) -> [1#, 2#, 3#, 4#]', () => {
    expect(myMap<number, string>(x => x + '#', [1, 2, 3, 4])).toEqual([
      '1#',
      '2#',
      '3#',
      '4#'
    ]);
  });
});

describe('Filter', () => {
  test('myFilter<number, boolean>(x => x % 2 === 0, [1, 2, 3, 4, 5, 6]) -> [2, 4, 6]', () => {
    expect(
      myFilter<number, boolean>(x => x % 2 === 0, [1, 2, 3, 4, 5, 6])
    ).toEqual([2, 4, 6]);
  });

  test('myFilter<number, boolean>(x => x > 7, [1, 2, 3, 4, 5, 6] -> []', () => {
    expect(myFilter<number, boolean>(x => x > 7, [1, 2, 3, 4, 5, 6])).toEqual(
      []
    );
  });
});

describe('Reduce', () => {
  test('myReduce<number>((x, y) => x + y, 0, [1, 2, 3, 4]) -> 10', () => {
    expect(myReduce<number>((x, y) => x + y, 0, [1, 2, 3, 4])).toBe(10);
  });
  test('myReduce<number>((x, y) => x * y, 1, [1, 2, 3, 4]) -> 24', () => {
    expect(myReduce<number>((x, y) => x * y, 1, [1, 2, 3, 4])).toBe(24);
  });

  test('myReduce<number>((x, y) => x * y, 1, []) -> 1', () => {
    expect(myReduce<number>((x, y) => x * y, 1, [])).toBe(1);
  });
});
